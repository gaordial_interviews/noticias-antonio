<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	//protected $fillable = ['names', 'surnames', 'email', 'telephone', 'state'];

	/**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public static function getNews()
    {
        $xml = simplexml_load_file('http://www.antonionarino.gov.co/rss/noticias');
		// $json = json_encode($xml);
		// $xmlArr = json_decode($json, true);
		// $items = $xmlArr['channel']['item'];
        // dd($items); exit;
        return $xml;
    }
}
