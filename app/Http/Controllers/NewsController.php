<?php

namespace App\Http\Controllers;

// use App\Activity;
use Illuminate\Http\Request;

class NewsController extends Controller
{
	public function index()
	{
		if (!isset($res))
			$res = new \stdClass();

		$res->title = 'Noticias de Antonio Nariño';
		$xml = \App\News::getNews();
		// dd($xml);		
		foreach ($xml->channel->item as $ki => $item) {
			if(strlen($item->description) >= 100) {
				$item->desc = mb_substr($item->description, 0, 100);
				$item->desc .= ' <button class="btn btn-sm btn-info" title="' . $item->description . '">...</button>';
			} else {
				$item->desc = $item->description;
			}
		}
		return view('news.index', compact('res', 'xml'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Interview  $responsable
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if (!isset($res))
			$res = new \stdClass();

		$res->title = 'Ver entrevista';
		$interview = \App\Interview::where('id', $id)->firstOrFail();
		//dd($interviews);
		if($interview != null) {
		   if($interview->estado == 1) {
			   $interview->nombre_estado = 'Activo';
		   } else if($interview->estado == 2) {
			   $interview->nombre_estado = 'Inactivo';
		   }
		}
		return view('interviews.show', compact('res', 'interview'));
	}

	/**
	 * Shows the information of the search.
	 *
	 * @param  \Illuminate\Http\Request;
	 * @return
	 */
	public function list($id) {
		$interviews = \App\interviews::with(['activities'])->where('id', $id)->firstOrFail();
		return Datatables::of($interviews->activities)
				->editColumn('state', function($interviews) {
					$interviews->state = ($interviews->state == 1) ? 'Activo': 'Inactivo';
					return $interviews->state;
				})
				->addColumn('opc', function($interviews) {
					$column = '';
					return $column;
				})
				->escapeColumns([])
				->make(true);
				//->toJson();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Responsable  $responsable
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Responsable $responsable)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Responsable  $responsable
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Responsable $responsable)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Responsable  $responsable
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Responsable $responsable)
	{
		//
	}
}
