@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-md-12 pt-2">
			<h2 class="text-center text-primary font-weight-bold"><a href="{{ $xml->channel->link }}" target="_blank">{{ $xml->channel->title }}</a></h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<p>{{ $xml->channel->description }}</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<p><label class="font-weight-bold">Última fecha de actualización:</label> {{ $xml->channel->lastBuildDate }}</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table id="articlesTable" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">Título</th>
						<th class="text-center">Descripción</th>
						<th class="text-center">Autor</th>
						<th class="text-center">Categoría</th>
						<th class="text-center">Enlace</th>
					</tr>
				</thead>
				@if (!empty($xml))
					<tbody>
					@foreach ($xml->channel->item as $item)
						<tr>
							<td>{{ $item->title }}</td>
							<td>{!! $item->desc !!}</td>
							<td>{{ $item->author }}</td>
							<td>{{ $item->category }}</td>
							<td><a href="{{ $item->link }}" target="_blank">Ver</a></td>
						</tr>
					@endforeach
					</tbody>
				@endif
			</table>
		</div>
	</div>
@endsection
