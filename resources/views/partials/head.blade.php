	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="baseurl" content="{{ url('') }}" />

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
	<title>{{ (isset($res->title)) ? $res->title . ' - ': '' }}{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	@isset($arrCss)
	    @foreach ($arrCss as $css)
	        <link href="{{ $css }}" rel="stylesheet">
	    @endforeach
	@endisset
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ url('vendor/dataTables/jquery.dataTables.min.css') }}">
