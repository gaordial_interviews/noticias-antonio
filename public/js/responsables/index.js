$(function() {
    var baseurl = $('meta[name="baseurl"]').attr("content") + '/';

    $('[data-toggle="tooltip"]').tooltip();

    var responsablesTable = $('#responsablesTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: baseurl + 'responsables/list/1',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'description', name: 'description' },
            { data: 'deadline', name: 'deadline' },
            { data: 'name_state', name: 'name_state' },
            { data: 'opc', name: 'opc', orderable: false, searchable: false }
        ],
        'language': {
            'url': baseurl + 'vendor/dataTables/datatables.locale-es.json',
        },
        'paging': true,
        'pageLength': 50,
        'bFilter': true,
        'ordering': false,
        'responsive': true,
        'searching': false,
        'info': false
    });
});